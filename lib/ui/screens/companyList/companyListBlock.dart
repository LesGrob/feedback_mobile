import 'dart:async';
import 'package:feedback_mobile/models/company.dart';
import 'package:feedback_mobile/services/apiService.dart';

class CompanyListBlockState {
  bool loading;
  List<Company> companies;

  CompanyListBlockState(this.loading, this.companies);

  CompanyListBlockState.empty() {
    loading = false;
    companies = new List();
  }
}

class CompanyListBlock {
  ApiService apiService = ApiService();

  CompanyListBlockState _currentState;

  final _companyListController =
      StreamController<CompanyListBlockState>.broadcast();
  Stream<CompanyListBlockState> get companyListStream =>
      _companyListController.stream;

  CompanyListBlock() {
    _currentState = CompanyListBlockState.empty();
  }

  CompanyListBlockState getCurrentState() {
    return _currentState;
  }

  fetchCompanies() {
    _currentState.loading = true;
    apiService.companyApiService.getCompanies().then((response) {
      if (response.success) {
        _currentState.companies = response.data;
      }
      _currentState.loading = false;
      _companyListController.add(_currentState);
    });
  }
}
