import 'package:feedback_mobile/models/company.dart';
import 'package:feedback_mobile/services/sharedService.dart';
import 'package:feedback_mobile/ui/components/rows/companyListRow.dart';
import 'package:feedback_mobile/ui/screens/companyList/companyListBlock.dart';
import 'package:feedback_mobile/ui/screens/companyList/companyListBlockProvider.dart';
import 'package:flutter/material.dart';

class CompanyListBuilder {
  CompanyListBlock bloc;

  CompanyListBuilder() {
    bloc = CompanyListBlock();
  }
  CompanyListBuilder.fromBlock({this.bloc});

  CompanyListBlockProvider provider() {
    return CompanyListBlockProvider(
        bloc: bloc, child: CompanyListScreen(title: "Список компаний"));
  }
}

class CompanyListScreen extends StatefulWidget {
  final String title;

  CompanyListScreen({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CompanyListScreenState();
}

class CompanyListScreenState extends State<CompanyListScreen> {
  bool _firstTimeLoad = true;

  void logout() {
    SharedService.logout(context);
  }

  @override
  Widget build(BuildContext context) {
    CompanyListBlock block = CompanyListBlockProvider.of(context).bloc;

    if (_firstTimeLoad) {
      block.fetchCompanies();
      _firstTimeLoad = false;
    }

    return Scaffold(
      appBar: AppBar(title: Text(widget.title), actions: <Widget>[
        // action button
        IconButton(
          icon: Icon(Icons.exit_to_app),
          onPressed: logout,
        ),
      ]),
      body: StreamBuilder<CompanyListBlockState>(
        initialData: block.fetchCompanies(),
        stream: block.companyListStream,
        builder: _buildBody,
      ),
    );
  }

  Widget _buildBody(
      BuildContext context, AsyncSnapshot<CompanyListBlockState> snapshot) {
    if (snapshot.data == null || snapshot.data.loading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return CompanyListView(
      companies: snapshot.data.companies,
      refreshCallback: _handleRefresh,
    );
  }

  Future<Null> _handleRefresh() async {
    CompanyListBlock bloc = CompanyListBlockProvider.of(context).bloc;
    bloc.fetchCompanies();
    await bloc.companyListStream.first;
    return null;
  }
}

//Screen builder
class CompanyListView extends StatelessWidget {
  final List<Company> companies;
  final RefreshCallback refreshCallback;

  CompanyListView({this.companies, this.refreshCallback});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refreshCallback,
      child: _companiesList(),
    );
  }

  ListView _companiesList() {
    var widgets = <Widget>[];
    for (var i = 0; i < companies.length; i++) {
      widgets.add(CompanyListRow(company: companies[i]));
    }

    return ListView(
        physics: const AlwaysScrollableScrollPhysics(), children: widgets);
  }
}
