import 'package:feedback_mobile/ui/screens/companyList/companyListBlock.dart';
import 'package:flutter/material.dart';

class CompanyListBlockProvider extends InheritedWidget {
  final CompanyListBlock bloc;
  final Widget child;

  CompanyListBlockProvider({this.bloc, this.child}) : super(child: child);

  static CompanyListBlockProvider of(BuildContext context) =>
      context.inheritFromWidgetOfExactType(CompanyListBlockProvider);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}
