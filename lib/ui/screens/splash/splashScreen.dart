import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    countDownTime();
  }

  final int splashDuration = 2;

  countDownTime() async {
    return Timer(Duration(seconds: splashDuration), () {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      SharedPreferences.getInstance().then((preferences) {
        final token = preferences.getString("token");
        if (token != null && token != "") {
          Navigator.of(context).pushReplacementNamed('/CompanyListScreen');
        } else {
          Navigator.of(context).pushReplacementNamed('/LoginScreen');
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.white));

    return Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("INeedChat",
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 36.0,
                          color: Colors.white)),
                ],
              ),
            ),
          ),
        ));
  }
}
