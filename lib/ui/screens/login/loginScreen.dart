import 'package:feedback_mobile/directories/configuration.dart';
import 'package:feedback_mobile/services/apiService.dart';
import 'package:feedback_mobile/ui/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  void initState() {
    super.initState();
    // apiService = new ApiService(context);
    phoneController.text = "79523401124";
    codeController.text = "111111";
  }

  ApiService apiService = new ApiService();
  final phoneController = TextEditingController();
  final codeController = TextEditingController();

  void onCheckButtonClick() {
    var tel = phoneController.text;
    apiService.authenticationApiService.identification(tel).then((data) async {
      if (data.success) {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        await preferences.setString("imei", data.data.imei);
        showErrorDialog(context, "Code was send");
      } else {
        showErrorDialog(context, data.errorMessage);
      }
    });
  }

  void onLoginButtonClick() {
    var tel = phoneController.text;
    var code = codeController.text;
    apiService.authenticationApiService.authentication(tel, code).then((data) {
      if (data.success) {
        apiService.authenticationApiService
            .saveToken(data.data)
            .then((success) {
          showErrorDialog(context, "Saved");
          Navigator.of(context).pushReplacementNamed('/CompanyListScreen',
              arguments: (Route<dynamic> route) => false);
        });
      } else {
        showErrorDialog(context, data.errorMessage);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.black));

    final phoneField = TextField(
      obscureText: false,
      style: style,
      controller: phoneController,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Телефон",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final codeField = TextField(
      obscureText: false,
      style: style,
      controller: codeController,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "SMS код",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final checkButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Configuration.mainColor,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: onCheckButtonClick,
        child: Text("Получить код",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Configuration.mainColor,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: onLoginButtonClick,
        child: Text("Вход",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
        body: Center(
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                  height: 155.0,
                  child: Image.asset("assets/logo.png", fit: BoxFit.contain)),
              SizedBox(height: 45.0),
              phoneField,
              SizedBox(height: 25.0),
              codeField,
              SizedBox(height: 35.0),
              checkButton,
              SizedBox(height: 15.0),
              loginButton,
              SizedBox(height: 15.0)
            ],
          ),
        ),
      ),
    ));
  }
}
