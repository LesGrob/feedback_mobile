import 'package:feedback_mobile/models/company.dart';
import 'package:flutter/widgets.dart';

class CompanyListRow extends StatelessWidget {
  final Company company;

  CompanyListRow({this.company});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(top: 8.0, left: 8.0),
              child: Text(company.name,
                  style: TextStyle(fontWeight: FontWeight.bold)))
        ],
      ),
    );
  }
}
