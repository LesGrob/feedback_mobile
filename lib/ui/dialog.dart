import 'package:flutter/material.dart';

void showErrorDialog(BuildContext context, String message) {
  showCustomDialog(context, "Ошибка", message, "Ок");
}

void showCustomDialog(
    BuildContext context, String title, String message, String buttonLabel) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: new Text(title),
        content: new Text(message),
        actions: <Widget>[
          new FlatButton(
            child: new Text(buttonLabel),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
