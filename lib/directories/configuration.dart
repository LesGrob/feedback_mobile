import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Configuration {
  static const mainColor = Color.fromRGBO(252, 77, 108, 1);

  static final theme = ThemeData(
    brightness: Brightness.light,
    primaryColor: mainColor,
    accentColor: mainColor,
    primarySwatch: Colors.pink,
    fontFamily: 'Montserrat',
    
    // textTheme: TextTheme(
    //   headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
    //   title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
    //   body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
    // ),
  );
}