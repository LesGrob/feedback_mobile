import 'package:feedback_mobile/directories/configuration.dart';
import 'package:feedback_mobile/ui/screens/companyList/companyListScreen.dart';
import 'package:feedback_mobile/ui/screens/login/loginScreen.dart';
import 'package:feedback_mobile/ui/screens/splash/splashScreen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: Configuration.theme,
      routes: <String, WidgetBuilder>{
        "/CompanyListScreen": (BuildContext context) =>
            CompanyListBuilder().provider(),
        "/LoginScreen": (BuildContext context) => LoginScreen(),
      },
      home: SplashScreen(),
    );
  }
}
