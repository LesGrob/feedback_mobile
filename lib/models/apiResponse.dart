class ApiResponse<T> {
  int errorCode;
  String errorMessage;
  bool success;
  T data;

  ApiResponse(this.errorCode, this.errorMessage, this.success, this.data);
  ApiResponse.fromJson(Map<String, dynamic> json)
      : errorCode = json['ErrorCode'],
        errorMessage = json['ErrorMessage'],
        success = json['Success'];
}
