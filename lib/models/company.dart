import 'dart:convert';

import 'package:feedback_mobile/models/apiResponse.dart';

class CompanyResponse<T> extends ApiResponse<Company> {
  CompanyResponse.fromJson(Map<String, dynamic> jsonData)
      : super.fromJson(jsonData) {
    this.data =
        jsonData['Data'] == null ? null : Company.fromJson(jsonData['Data']);
  }
}

class CompaniesResponse<T> extends ApiResponse<List<Company>> {
  CompaniesResponse.fromJson(Map<String, dynamic> jsonData)
      : super.fromJson(jsonData) {
    if (jsonData['Data'] == null) this.data = null;
    List<Company> companies = new List<Company>();
    final List arr = jsonData["Data"];
    arr.forEach((i) {
      companies.add(new Company.fromJson(i));
    });
    this.data = companies;
  }
}

class Company {
  int id;
  String name;
  String description;
  String logotype;
  String address;
  String slogan;
  bool isActive;
  String externalId;
  String inn;

  Company(this.id, this.name, this.description, this.logotype, this.address,
      this.slogan, this.isActive, this.externalId, this.inn);
  Company.fromJson(Map<String, dynamic> jsonData)
      : id = jsonData["Id"] as int,
        name = jsonData["Name"],
        description = jsonData["Description"],
        logotype = jsonData["Logotype"],
        address = jsonData["Address"],
        slogan = jsonData["Slogan"],
        isActive = jsonData["IsActive"] as bool,
        externalId = jsonData["ExternalId"],
        inn = jsonData["Inn"];
}

///////////////////

class CityResponse extends ApiResponse<City> {
  CityResponse.fromJson(Map<String, dynamic> jsonData)
      : super.fromJson(jsonData) {
    this.data =
        jsonData['Data'] == null ? null : City.fromJson(jsonData['Data']);
  }
}

class City {
  int id;
  String name;

  City(this.id, this.name);
  City.fromJson(Map<String, dynamic> json)
      : id = json["Id"] as int,
        name = json["Name"];
}
