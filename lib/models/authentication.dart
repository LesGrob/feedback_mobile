import 'apiResponse.dart';

class IdentificationResponse extends ApiResponse<Identification> {
  IdentificationResponse.fromJson(Map<String, dynamic> jsonData)
      : super.fromJson(jsonData) {
    this.data = jsonData['Data'] == null
        ? null
        : Identification.fromJson(jsonData['Data']);
  }
}

class Identification {
  int phoneNumber;
  String imei;
  int verifiedCode;

  Identification(this.phoneNumber, this.imei, this.verifiedCode);
  Identification.fromJson(Map<String, dynamic> jsonData)
      : phoneNumber = jsonData['phoneNumber'] as int,
        imei = jsonData['IMEI'] as String,
        verifiedCode = jsonData['verifiedCode'] as int;
}

///////////////////

class AuthenticationResponse extends ApiResponse<Authentication> {
  AuthenticationResponse.fromJson(Map<String, dynamic> jsonData)
      : super.fromJson(jsonData) {
    this.data = jsonData['Data'] == null
        ? null
        : Authentication.fromJson(jsonData['Data']);
  }
}

class Authentication {
  int id;
  String appeal;
  String surname;
  String name;
  String middlename;
  String email;
  String birthday;
  String gender;
  String logotype;
  String token;

  Authentication(this.id, this.appeal, this.surname, this.name, this.middlename,
      this.email, this.birthday, this.gender, this.logotype, this.token);

  Authentication.fromJson(Map<String, dynamic> jsonData)
      : id = jsonData['Id'] as int ?? 0,
        appeal = jsonData['Appeal'] as String,
        surname = jsonData['Surname'] as String,
        name = jsonData['Name'] as String,
        middlename = jsonData['Middlename'] as String,
        email = jsonData['Email'] as String,
        birthday = jsonData['Birthday'] as String,
        gender = jsonData['Gender'] as String,
        logotype = jsonData['Logotype'] as String,
        token = jsonData['Token'] as String;
}
