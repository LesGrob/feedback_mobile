import 'package:feedback_mobile/models/apiResponse.dart';
import 'package:feedback_mobile/models/authentication.dart';
import 'package:feedback_mobile/services/httpService.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationApiService extends HttpService {
  // AuthenticationApiService(BuildContext context) : super(context);

  Future<IdentificationResponse> identification(String phoneNumber) async {
    final body = {"phoneNumber": phoneNumber};
    final headers = {"": ""};
    final data = await makeObservableRequest(
        "AccessControl/Identification", body, headers, RequestType.get);
    return new IdentificationResponse.fromJson(data);
  }

  Future<AuthenticationResponse> authentication(
      String phoneNumber, String verifiedCode) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final imei = preferences.getString("imei");
    final body = {"phoneNumber": phoneNumber, "verifiedCode": verifiedCode};
    final headers = {"Cookie": "IMEI=" + imei};
    final data = await makeObservableRequest(
        "AccessControl/Authentication", body, headers, RequestType.get);
    return new AuthenticationResponse.fromJson(data);
  }

  Future<ApiResponse> logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final imei = preferences.getString("imei");
    if (imei == null) return null;
    final body = null;
    final headers = {"Cookie": "IMEI=" + imei};
    final data = await makeObservableRequest(
        "AccessControl/Logout", body, headers, RequestType.get);
    return new ApiResponse.fromJson(data);
  }

  Future<bool> saveToken(Authentication data) async {
    if (data.token == null || data.token == "") return false;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString("token", data.token);
    return true;
  }

  Future<bool> removeData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove("token");
    await preferences.remove("imei");
    return true;
  }
}
