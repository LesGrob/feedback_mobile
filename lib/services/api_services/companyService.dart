import 'package:feedback_mobile/models/apiResponse.dart';
import 'package:feedback_mobile/models/company.dart';
import 'package:feedback_mobile/services/httpService.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CompanyApiService extends HttpService {
  // CompanyApiService(BuildContext context) : super(context);

  Future<CompaniesResponse> getCompanies() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final token = preferences.getString("token");
    if (token == null && token == "")
      return new ApiResponse(0, "No token", false, null) as CompaniesResponse;
    final body = {"": ""};
    final headers = {"Authorization": token};
    final data = await makeObservableRequest(
        "Admin/GetCompanies", body, headers, RequestType.get);
    return new CompaniesResponse.fromJson(data);
  }

  Future<CityResponse> getCity() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final token = preferences.getString("token");
    if (token == null && token == "")
      return new ApiResponse(0, "No token", false, null) as CityResponse;
    final body = {"localityId": "1"};
    final headers = {"Authorization": token};
    final data = await makeObservableRequest(
        "Admin/GetLocality", body, headers, RequestType.get);
    return new CityResponse.fromJson(data);
  }
}
