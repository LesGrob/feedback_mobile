import 'package:feedback_mobile/services/api_services/authService.dart';
import 'package:feedback_mobile/services/api_services/companyService.dart';
import 'package:flutter/cupertino.dart';

class ApiService {
  // BuildContext context;

  // ApiService(BuildContext context) {
  //   this.context = context;
  //   authenticationApiService = AuthenticationApiService(context);
  //   companyApiService = CompanyApiService(context);
  // }

  AuthenticationApiService authenticationApiService = AuthenticationApiService();
  CompanyApiService companyApiService = CompanyApiService();
}
