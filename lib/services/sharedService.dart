import 'package:feedback_mobile/services/apiService.dart';
import 'package:flutter/cupertino.dart';

class SharedService {
  static void logout(BuildContext context) {
    ApiService apiService = new ApiService();
    apiService.authenticationApiService.logout().then((data) async {
      await apiService.authenticationApiService.removeData();
      Navigator.of(context).pushReplacementNamed('/LoginScreen');
    });
  }
}
