import 'dart:convert';
import 'package:feedback_mobile/models/apiResponse.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

enum RequestType { get, post }

class HttpService {
  String currentUrl = "testineedchat.iserve.chat";
  String apiUrl = "/api/LP/v3/";
  // BuildContext context;

  // HttpService(this.context) {
  //   this.context = context;
  // }

  Future<http.Response> makeRequest(String url, Map<String, dynamic> body,
      Map<String, String> headers, RequestType requestType) async {
    Uri uri = new Uri.https(currentUrl, apiUrl + url, body);
    if (requestType == RequestType.get) {
      return http.get(uri, headers: headers);
    }
    return http.post(uri, body: body, headers: headers);
  }

  Future<Map<String, dynamic>> makeObservableRequest(
      String url,
      Map<String, dynamic> body,
      Map<String, String> headers,
      RequestType requestType) async {
    final response = await makeRequest(url, body, headers, requestType);
    if (response.statusCode == 200) {
      final data = ApiResponse.fromJson(json.decode(response.body));
      // if (!data.success) {
      //   throw Exception(
      //       "Request failed: " + url + ", error:" + data.errorMessage);
      // }
      return json.decode(response.body);
    } else {
      throw Exception("Request failed: " +
          url +
          ", error:" +
          response.statusCode.toString());
    }
  }
}
